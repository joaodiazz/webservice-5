# Servidor Fake

Para ativar o servidor, use o comando abaixo no seu prompt de comandos.

```
npm i -g json-server
```

Para ligar o servidor fake, entre na pasta do projeto no terminal e use o comando abaixo.

```
json-server books.json
```

Pegue o URL gerado e use no [Postman](https://www.postman.com), e faça as operações CRUD que deseja.
